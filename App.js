import React from 'react';
import { StyleSheet, View, Text,Button } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { AppRegistry, Image } from 'react-native';

let pic = 
{
  uri: 'https://upload.wikimedia.org/wikipedia/commons/a/a2/Ameesha_at_the_Cannes_Film_Festival.jpg'
};

const HomeScreen = ({ navigation }) => (
  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <Text style={{fontSize:30}}>Home Screen</Text>
    
          <Image source={pic} style={styles.container2}/>
        
    <Button
      onPress={() => navigation.navigate('Images')}
      title="Go to Images"
      />
      <Button
      onPress={() => navigation.navigate('Details')}
      title="Go to details"
      />
  </View>
);

const DetailsScreen = ({ navigation }) => (
  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <Text style={{fontSize:30}}>Details Screen</Text>
    
          <Image source={pic} style={styles.container2}/>
       
    <Button
      onPress={() => navigation.navigate('Home')}
      title="Go to home screen"
      />
      <Button
      onPress={() => navigation.navigate('Images')}
      title="Go to Images"
      />
  </View>
);
const ImgScreen = ({ navigation }) => (
  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <Text style={{fontSize:30}}>Image Screen</Text>
   
          <Image source={pic} style={styles.container2}/>
     
    <Button
      onPress={() => navigation.navigate('Details')}
      title="Go to details"
      />
      <Button
      onPress={() => navigation.navigate('Home')}
      title="Go to home screen"
      />
      
  </View>
);

const RootNavigator = StackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      headerTitle: 'Home',
    },
  },
  Details: {
    screen: DetailsScreen,
    navigationOptions: {
      headerTitle: 'Details',
    },
  },
  Images: {
    screen: ImgScreen,
    navigationOptions: {
      headerTitle: 'Images',
    },
  }
});

const styles = StyleSheet.create(
  {
  container2:
  {
    width: 293, 
    height: 250,
    alignItems: 'center',
    justifyContent: 'center',
  },
  
  container7:
  {
    flex:3,
    backgroundColor: 'green'

  }
});

export default RootNavigator;